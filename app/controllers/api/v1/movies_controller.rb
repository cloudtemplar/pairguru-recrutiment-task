class API::V1::MoviesController < ApplicationController
  respond_to :json

  def index
    movies = Movie.all
    respond_with(movies, each_serializer: MovieSerializerV1)
  end

  def show
    movie = Movie.find(params[:id])
    respond_with(movie, serializer: MovieSerializerV1)
  end
end
