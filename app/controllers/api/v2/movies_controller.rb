class API::V2::MoviesController < ApplicationController
  respond_to :json

  def index
    movies = Movie.all
    respond_with(movies, each_serializer: MovieSerializerV2)
  end

  def show
    movie = Movie.find(params[:id])
    respond_with(movie, serializer: MovieSerializerV2)
  end
end
