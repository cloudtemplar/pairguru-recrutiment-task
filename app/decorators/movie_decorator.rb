class MovieDecorator < Draper::Decorator
  delegate_all

  def self.collection_decorator_class
    PaginatingDecorator
  end

  def get_info(attr_name)
    @movie_info ||= MoviesAPIConsumer.new(object.title).movie_info
    if attr_name == 'poster'
      'https://pairguru-api.herokuapp.com' + @movie_info[attr_name]
    else
      @movie_info[attr_name]
    end
  end

  ATTRIBUTES = ['poster', 'plot', 'rating']

  ATTRIBUTES.each do |attr_name|
    define_method "#{attr_name}" do
      get_info(attr_name)
    end
  end
end
