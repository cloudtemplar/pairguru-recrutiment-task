class GenreSerializer < ActiveModel::Serializer
  attributes :id, :name, :movies_count
end
