class MovieSerializerV2 < ActiveModel::Serializer
  attributes :id, :title
  belongs_to :genre
end
