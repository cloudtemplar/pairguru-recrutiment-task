require 'httparty'

class MoviesAPIConsumer
  attr_reader :movie_info

  def initialize(title)
    raise ArgumentError if title.blank?
    base_url = 'https://pairguru-api.herokuapp.com'
    api_url   = base_url + '/api/v1'
    # Insert space entities
    normalized_title = title.gsub(' ', '%20')
    query = HTTParty.get(api_url + '/movies/' + normalized_title)
    @movie_info = query['data']['attributes']
  end
end
