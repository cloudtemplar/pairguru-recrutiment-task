class TitleBracketsValidator < ActiveModel::Validator
  def validate(record)
    title = record.title

    record_invalid = lambda do
      return record.errors.add(:title, "Brackets unmatched")
    end

    record_invalid.call if title =~ /(\(\)|\[\]|\{\})/

    brackets = {
      '(' => ')',
      '{' => '}',
      '[' => ']'
    }
    
    title_trim = title.gsub(/[^\[\]\(\)\{\}]/,'')
    record_invalid.call if title_trim.split('').size.odd?
    
    brackets_opened = []
    
    title_trim.each_char do |bracket|
      if bracket.match(/[\[|\{|\(]/)
        brackets_opened << bracket
      elsif bracket.match(/[\]|\}|\)]/)
        record_invalid.call if brackets[brackets_opened.pop] != bracket
      end
    end
  end
end
