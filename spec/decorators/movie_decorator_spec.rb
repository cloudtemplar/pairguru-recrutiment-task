require 'rails_helper'

describe MovieDecorator do
  let!(:movie) { FactoryGirl.create(:movie, title: 'Godfather').decorate }

  context '#poster' do
    it "returns movie's poster url" do
      expect(movie.poster).to eq "https://pairguru-api.herokuapp.com/godfather.jpg"
    end
  end

  context '#plot' do
    it "returns movie's plot" do
      expect(movie.plot).to include "The aging patriarch of an organized crime"
    end
  end

  context '#rating' do
    it "returns movie's rating" do
      expect(movie.rating).to eq 9.2
    end
  end
end
