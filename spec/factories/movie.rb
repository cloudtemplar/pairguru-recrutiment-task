FactoryGirl.define do
  factory :movie do
    movies = ["Godfather",
              "Star Wars V",
              "The Dark Knight",
              "Pulp Fiction",
              "Django",
              "Kill Bill 2",
              "Inception",
              "Inglourious Basterds",
              "Kill Bill",
              "Deadpool"
             ]
    title { movies.sample }
    description { Faker::Lorem.sentence(3, true) }
    released_at { Faker::Date.between(40.years.ago, Time.zone.today) }
    genre
  end
end
