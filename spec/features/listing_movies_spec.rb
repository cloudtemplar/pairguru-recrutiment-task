require 'rails_helper'

RSpec.feature 'Users can view a list of movies' do
  let!(:godfather) { FactoryGirl.create(:movie, title: 'Godfather') }
  let!(:starwars)  { FactoryGirl.create(:movie, title: 'Star Wars V') }

  scenario 'with proper images, plot overviews and ratings' do
    visit '/movies'
    expect(page).to have_content 'Godfather'
    expect(page).to have_content 'Star Wars V'
    # Check if movie descriptions are actual plots from Movie API.
    expect(page).to have_content 'The aging patriarch of an organized crime'
    expect(page).to have_content 'After the rebels have been brutally overpowered'
    # Ratings
    expect(page).to have_content '9.2'
    expect(page).to have_content '8.8'
  end
end
