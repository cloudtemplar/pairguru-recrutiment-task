require 'rails_helper'

RSpec.feature "User can view a movie's details" do
  let!(:godfather) { FactoryGirl.create(:movie, title: 'Godfather') }

  scenario 'With proper plot description and rating' do
    visit '/movies'
    click_link 'Godfather'

    within('h1') do
      expect(page).to have_content 'Godfather'
    end
    # Check if description and rating come from the MoviesAPI
    within('.description') do
      expect(page).to have_content 'The aging patriarch of an organized crime'
    end
    within('.rating') do
      expect(page).to have_content '9.2'
    end
  end
end
