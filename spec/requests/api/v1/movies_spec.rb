require 'rails_helper'

RSpec.describe "Movies API v1", type: :request do
  describe 'GET #index' do
    let!(:movies) { create_list(:movie, 5) }

    it "responds with body containing all movies" do
      get api_v1_movies_path, params: { format: :json }

      expect(response.status).to eq 200
      json = Movie.all.to_json(only: [:id, :title])
      expect(response.body).to eq json
    end
  end

  describe 'GET #show' do
    let(:godfather) { FactoryGirl.create(:movie, title: 'Godfather') }

    it "responds with body containing requested movie's details" do
      get api_v1_movie_path(godfather), params: { format: :json }

      expect(response.status).to eq 200
      json = godfather.to_json(only: [:id, :title])
      expect(response.body).to eq json
      expect(JSON.parse(response.body)['title']).to eq 'Godfather'
    end
  end
end
