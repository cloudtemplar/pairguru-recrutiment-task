require 'rails_helper'

RSpec.describe "Movies API v2", type: :request do
  describe 'GET #index' do
    let!(:movies) { create_list(:movie, 5) }

    it "responds with body containing all movies" do
      get api_v2_movies_path, params: { format: :json }

      expect(response.status).to eq 200
      json = JSON.parse(response.body)
      expect(json.count).to eq 5
      expect(json[0]).to include 'title'
      expect(json[0]).to include 'genre'
      expect(json[0]['genre']).to include 'movies_count'
    end
  end

  describe 'GET #show' do
    let(:godfather) { FactoryGirl.create(:movie, title: 'Godfather') }

    it "responds with body containing requested movie's and genre's details" do
      get api_v2_movie_path(godfather), params: { format: :json }

      expect(response.status).to eq 200
      json = JSON.parse(response.body)
      expect(json['title']).to eq 'Godfather'
      expect(json['genre']['name']).to be_a String
      expect(json['genre']['movies_count']).to be_a Integer
    end
  end
end
