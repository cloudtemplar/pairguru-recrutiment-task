require 'rails_helper'

describe MoviesAPIConsumer do
  subject { MoviesAPIConsumer }
  let(:consumer) { subject.new('Godfather') }

  it "expects movie title as an argument" do
    expect(subject).to respond_to(:new).with(1).argument
  end

  it "throws an error if no title passed" do
    expect { subject.new }.to raise_error(ArgumentError)
  end

  it "returns movie's info" do
    expect(consumer.movie_info).to eq ({ 
      "plot" => "The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.",
      "poster" => "/godfather.jpg",
      "rating" => 9.2,
      "title" => "Godfather"
    })
  end
end
